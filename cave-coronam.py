# This should be mostly unbreakable now

import os, json, requests, sys, time, math

print("Starting program...")

def fetch(url, json = True, attempt = 0):
    print("Fetching url <", url, "> as", "json" if json else "text", "...")
    try:
        req = requests.get(url)
        status = req.status_code
        if (status - 200) < 100:
            return req.json() if json else req.text
        elif attempt < 5:
            print("Could not fetch url <", url, "> with status code", status)
            sleep = 2**attempt
            print("Retrying in", sleep, "seconds...")
            time.sleep(sleep)
            return fetch(url, json, attempt + 1)
    except Exception as e:
        print("Exception has arisen while fetching:")
        repr(e)
    print("Returning empty after", attempt, "failed attempts to fetch")
    return {} if json else ""

def getAt(collection, index):
    return collection[index] if collection != None and index < len(collection) else None

def getOrNone(dictionary, *keys):
    toReturn = dictionary
    for key in keys:
        if toReturn and key in toReturn:
            toReturn = toReturn[key]
        else:
            return None
    return toReturn

def ifNoneThen(value, other):
    return other if value == None else value

print("Loading environment variables...")
envVars = {
    "webhooks": os.getenv("webhooks").split(";"),
    "counties": os.getenv("counties").split(";"),
    "state": os.getenv("state") or "BY",
    "custom": os.getenv("custom")
}
envVars["stateid"] = { "SH": 1, "HH": 2, "NI": 3, "HB": 4, "NW": 5, "HE": 6, "RP": 7, "BW": 8, "BY": 9, "SL": 10, "BE": 11, "BB": 12, "MV": 13, "SN": 14, "ST": 15, "TH": 16 }.get(envVars["state"], -1)

print("Fetching data...")
incidentRanges = fetch("https://api.corona-zahlen.org/map/districts/legend").get("incidentRanges", [{}])

class Embed:
    def __init__(self, title):
        self.title = title
        self.fields = []
        self.color = None

    def addField(self, name, value, inline=True):
        if value and ((not (isinstance(value, float) or isinstance(value, int))) or value > 0):
            self.fields.append({
                "name": str(name),
                "value": str(round(value) if isinstance(value, float) else value),
                "inline": inline is True
            })

    def setWeekIncidence(self, weekIncidence):
        if not isinstance(weekIncidence, float):
            try:
                weekIncidence = float(weekIncidence)
            except TypeError:
                return
        if weekIncidence != None and weekIncidence >= 0:
            self.addField("Inzidenz", weekIncidence)
            color = next(filter(lambda iRange: weekIncidence >= (iRange.get("min", 0) or 0) and weekIncidence <= ifNoneThen(iRange.get("max", None), math.inf), incidentRanges), {}).get("color", None)
            if color and len(color) > 1:
                try:
                    self.color = int(color[1:], 16)
                except TypeError:
                    return

    def addTo(self, message):
        if len(self.fields):
            embeds = message.get("embeds", None)
            if embeds != None:
                embed = {
                    "title": self.title,
                    "fields": self.fields,
                    "color": self.color
                }
                if self.timestamp:
                    embed["timestamp"] = self.timestamp
                embeds.append(embed)

# Message
message = {
    "content": "__**Covid-19 Zahlen**__",
    "embeds": []
}

# Currently only works for Bavaria
hospitalizationData = list(map(lambda line: line.split(","), fetch("https://docs.google.com/spreadsheets/d/1NFo_CO9KQrUkrLy8jOi5ZygfmgRYsRlrCrMGI6okLsA/export?format=csv&id=1NFo_CO9KQrUkrLy8jOi5ZygfmgRYsRlrCrMGI6okLsA&gid=469703850", False).split("\n")))
hospitalized = getAt(getAt(hospitalizationData, 1), 2)
icu = getAt(getAt(hospitalizationData, 2), 2)

# State
stateData = fetch("https://api.corona-zahlen.org/states/" + envVars["state"])
divi = getOrNone(getAt(getOrNone(fetch("https://www.intensivregister.de/api/public/reporting/laendertabelle"), "data"), envVars["stateid"]), "bettenBelegtToBettenGesamtPercent")
state = Embed(getOrNone(stateData, "data", envVars["state"], "name") or envVars["state"])
stateWeekIncidence = getOrNone(stateData, "data", envVars["state"], "weekIncidence")
state.setWeekIncidence(stateWeekIncidence)
if envVars["state"] == "BY":
    state.addField("Hospitalisierung", hospitalized)
    state.addField("Intensivstation", icu)

state.addField("Belegte Betten %", divi)
state.timestamp = getOrNone(stateData, "meta", "lastUpdate")
state.addTo(message)

# Districts
for ags in envVars["counties"]:
    districtData = fetch("https://api.corona-zahlen.org/districts/" + ags)
    districtDataNumbers = getOrNone(districtData, "data", ags)
    district = Embed(getOrNone(districtDataNumbers, "county") or ags)
    districtWeekIncidence = getOrNone(districtDataNumbers, "weekIncidence")
    district.setWeekIncidence(districtWeekIncidence)
    district.addField("Fälle", getOrNone(districtDataNumbers, "delta", "cases"))
    #district.addField("Genesene", getOrNone(districtDataNumbers, "delta", "recovered"))
    district.addField("Tode", getOrNone(districtDataNumbers, "delta", "deaths"))
    #if getOrNone(districtDataNumbers, "stateAbbreviation") == "BY":
    #    district.addField("3G", "Ja" if districtWeekIncidence > 35 else "Nein")
    district.timestamp = getOrNone(districtData, "meta", "lastUpdate")
    district.addTo(message)
    
# Customization
if not envVars.get("custom", False):
    message["avatar_url"] = "https://cdn.discordapp.com/attachments/954035345412993114/954431190717300796/Cave_Coronam.png"
    message["username"] = "Cave Coronam"

print("Message:")
print(json.dumps(message))

print("Sending message...")
for webhook in envVars["webhooks"]:
    response = requests.post(webhook, json = message, headers = { "ContentType": "application/json" })
    if response.status_code == 204:
        print("Successfully sent message")
    else:
        print("Could not send message. Error code: ")
        print(response)
